async function register ({
  registerHook,
  storageManager,
}) {
  const fieldName = 'message-info'
  
  // Store data associated to this video
  registerHook({
    target: 'action:api.video.updated',
    handler: ({ video, body }) => {
      if (!body.pluginData) return

      const randomText = body.pluginData[fieldName]
      if (!randomText) return

      storageManager.storeData(fieldName + '-' + video.id, randomText)
     
    }
  })

  // Add your custom value to the video, so the client autofill your field using the previously stored value
  registerHook({
    target: 'filter:api.video.get.result',
    handler: async (video) => {
      if (!video) return video
      if (!video.pluginData) video.pluginData = {}

      const result = await storageManager.getData(fieldName + '-' + video.id)
      video.pluginData[fieldName] = result

      return video
    }
  })

}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
