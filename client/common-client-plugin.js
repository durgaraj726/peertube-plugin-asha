async function register ({ registerHook, peertubeHelpers, registerVideoField }) {
  const fieldName = 'message-info'
  const descriptionSource = 'Sample Plugin <a href="https://terakoya-academia.com" target="_blank">Terakoya</a> Academia'
  const link = "https://terakoya-academia.com"

  const descriptionHTML = await peertubeHelpers.translate(descriptionSource)
  const commonOptions = {
    name: fieldName,
    label: 'Message',
    descriptionHTML,
    type: 'input',
    default: ''
  }

  for (const type of [ 'upload', 'import-url', 'import-torrent', 'update' ]) {
    registerVideoField(commonOptions, { type })

  }

  registerHook({
    target: 'action:video-watch.video.loaded',
    handler: ({video}) => {
      const randomText = video.pluginData[fieldName]
      console.log("This is random.", randomText);
      peertubeHelpers.showModal({
        title: 'Message',
        content: randomText,
        close: true,
        // show cancel button and call action() after hiding modal
        cancel: { value: 'cancel', action: () => {} },
        // show confirm button and call action() after hiding modal
        confirm: { value: 'confirm', action: () => {window.open(link, "_blank")} },
      })
    }
  })


  // registerHook({
  //   target:'action:video-edit.init',
  //   handler: () => {
  //     console.log(fieldName)
  //   }
  // })


}

export {
  register
}
